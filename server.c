#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXLINE 512
#define SERVER_PORT 7878
#define SHELL_TOK_BUFSIZE 64
#define SHELL_TOK_DELIM " \t\r\n"
#define MY_PATH "PATH=bin:."
#define DEFAULT_DIR "/home/mbl/ras"
#define SWAP( a, b ) { c = a; a = b; b = c; }
#define MAX_PIPE_NUM 2000

void err_dump( const char* x );
int readline( int fd, char *ptr, int maxlen );
void shell_loop( int ser_sockfd );
char *shell_read_line( int sockfd );
char **shell_split_line( char *line );
int shell_execute( char **args );
int shell_launch_command( char **args );
int setenvPATH( char **args );
int printenv( char **args );
void redirection( char **args, int filename_pos );
int calArgsLen( char **args );
char **shell_split_pipe( char *line );
int dealpipe( char **commands, int commandNum, int table[][3], int tableRow, int zeroCounterRow, int exclamation );
void initialPipeTable( int table[][3] );
int containNum( char **commands, int commandNum );
int discoverEmptyEntryInTable( int table[][3] );
int findSameCounter( int table[][3], int counter );
void subOneToCounter( int table[][3] );
int findErrNum( char **commands, int commandNum );

int main(int argc, char const *argv[]) {
    int sockfd, newsockfd, chilen, childpid, ser_port;
    struct sockaddr_in cli_addr, serv_addr;
    char welcome[3][42] = {"****************************************\n",
    "** Welcome to the information server. **\n",
    "****************************************\n"};

    if( argv[1] != NULL ){
        ser_port = atoi( argv[1] );
    }
    else{
        ser_port = SERVER_PORT;
    }

    /* enlarge the fd table */
    struct rlimit r;
    r.rlim_cur=2100;
    r.rlim_max=2500;

    if (setrlimit(RLIMIT_NOFILE,&r)<0){
        fprintf(stderr,"setrlimit error\n");
        exit(1);
    }

    /* Open a TCP socket ( an Internet stream socket ). */
    if( ( sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 ){
        err_dump("server: can't open stream socket");
    }

    /* Bind local address so that the client can send to us. */
    bzero(( char* ) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    serv_addr.sin_port = htons( ser_port );


    if( bind( sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0 ){
        err_dump("server: can't bind local address");
    }

    listen(sockfd, 5);

    // Create socket and accept
    for(;;){
        chilen = sizeof( cli_addr );
        newsockfd = accept( sockfd, (struct sockaddr*) &cli_addr, &chilen );
        if( newsockfd < 0 ) err_dump("server: accept error");

        if( (childpid = fork()) < 0 ) err_dump("server: fork error");
        else if( childpid == 0 ){
            /* child process */

            /* close original socket */
            close( sockfd );


            /* change stdout to newsockfd */
            dup2( newsockfd, STDOUT_FILENO );
            /* change stderr to newsockfd */
            dup2( newsockfd, STDERR_FILENO );

            /* change to default dir */
            chdir( DEFAULT_DIR );
            chroot( DEFAULT_DIR );

            /* initial the path */
            putenv( MY_PATH );

            /* print the welcome messages */
            for( int i = 0; i < 3; i++ ){
                int n = strlen( welcome[i] );
                if( write( newsockfd, welcome[i], n ) != n )
                err_dump("writen error");
            }

            /* create shell */
            shell_loop( newsockfd );

            exit(0);
        }
        else{
            close( newsockfd ); /* parent process */
            wait( NULL );
        }
    }
    close( sockfd );


    return 0;
}

// print the error messages
void err_dump( const char* x ){
    perror(x);
    exit(1);
}

// create shell and listen commands
void shell_loop( int ser_sockfd ){
    char *line;
    char **args;
    char **commands;
    int status = 1;
    int commandNum;
    int pipeTable[MAX_PIPE_NUM][3] = {0};
    int pipeCounter = 0;
    int sameCounterRow = -1;
    int insertPipeRow = -1;
    int pipefd[2];
    int zeroCounterRow = -1;
    int errNum = 0;
    int exclamation = 0;

    initialPipeTable( pipeTable );

    while( status ){

        write( ser_sockfd, "% ", 2 );

        // substact one to every counter in table
        subOneToCounter( pipeTable );

        // find the zero counter
        zeroCounterRow = findSameCounter( pipeTable, 0 );

        line = shell_read_line( ser_sockfd );

        // when shell_read_line doesn't read data
        if( strlen(line) <= 2 ){
            // err_dump("shell_loop: Connection Terminated");
            continue;
        }

        // parse commands with pipe symbol
        commands = shell_split_pipe( line );
        commandNum = calArgsLen( commands );

        // if( commandNum == 1 ){
        //     continue;
        // }

        errNum = findErrNum( commands, commandNum );

        if( errNum != 0 ){
            exclamation = 1;
        }
        else{
            exclamation = 0;
        }

        // char a[30];
        // sprintf( a, "%d", commandNum );
        // write( STDOUT_FILENO, a, strlen(a) );

        if( commandNum > 2 ){
            pipeCounter = containNum( commands, commandNum );
            if( pipeCounter != 0 ){
                // create pipe and put it to table
                sameCounterRow = findSameCounter( pipeTable, pipeCounter );
                if( sameCounterRow != -1 ){
                    // put it to the same pipe
                    status = dealpipe( commands, commandNum - 1, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                }
                else{
                    insertPipeRow = discoverEmptyEntryInTable( pipeTable );
                    // put the pipe inside table
                    if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                    pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                    pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                    pipeTable[insertPipeRow][2] = pipeCounter;
                    status = dealpipe( commands, commandNum - 1, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                }
            }
            else{
                // deal with no pipe numbers
                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        // status = dealpipe( commands, commandNum - 1, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                        status = dealpipe( commands, commandNum, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        // status = dealpipe( commands, commandNum - 1, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                        status = dealpipe( commands, commandNum, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
            }
        }
        else if( commandNum == 2 ){
            if( zeroCounterRow == -1 ){
                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        status = dealpipe( commands, commandNum, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        status = dealpipe( commands, commandNum, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
            }
            else{

                if( errNum != 0 ){
                    sameCounterRow = findSameCounter( pipeTable, errNum );
                    if( sameCounterRow != -1 ){
                        status = dealpipe( commands, commandNum, pipeTable, sameCounterRow, zeroCounterRow, exclamation );
                    }
                    else{
                        insertPipeRow = discoverEmptyEntryInTable( pipeTable );

                        // put the pipe inside table
                        if( pipe( pipefd ) < 0 ) err_dump("shell_loop: can't create pipes");
                        pipeTable[insertPipeRow][0] = pipefd[1]; // write pipe
                        pipeTable[insertPipeRow][1] = pipefd[0]; // read pipe
                        pipeTable[insertPipeRow][2] = errNum;
                        status = dealpipe( commands, commandNum, pipeTable, insertPipeRow, zeroCounterRow, exclamation );
                    }
                }
                else{
                    status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow, exclamation );
                }
                // status = dealpipe( commands, commandNum, pipeTable, -1, zeroCounterRow );
            }
        }

    }
}

// read the input commands
char *shell_read_line( int ser_sockfd ){
    char *line = NULL;
    ssize_t bufsize = 0;
    FILE *fp = fdopen( ser_sockfd, "r" ); /* convert file descriptor into FILE pointer (getline needs) */
    getline( &line, &bufsize, fp );
    // free( fp )
    return line;
}

char **shell_split_pipe( char *line ){
    int bufsize = SHELL_TOK_BUFSIZE, position = 0;
    char **tokens = (char**)malloc( bufsize * sizeof(char*) );
    char *token;

    if( !tokens ){
        err_dump( "shell_split_pipe: allocation error" );
    }

    /* split by pipe symbol */
    token = strtok( line, "|" );

    while( token != NULL ){
        tokens[position] = token;

        position++;

        if( position >= bufsize ){
            bufsize += SHELL_TOK_BUFSIZE;
            tokens = realloc( tokens, bufsize * sizeof(char*) );
            if( !tokens ){
                err_dump("shell_split_line: allocation error");
            }
        }

        token = strtok( NULL, "|" );
    }

    tokens[position] = NULL;

    return tokens;
}

/* Split the line user inputs. */
char **shell_split_line( char *line ){
    int bufsize = SHELL_TOK_BUFSIZE, position = 0;
    char **tokens = (char**)malloc( bufsize * sizeof(char*) );
    char *token;

    if( !tokens ){
        err_dump( "shell_split_line: allocation error" );
    }

    /* split by SHELL_TOK_DELIM */
    token = strtok( line, SHELL_TOK_DELIM );
    while( token != NULL ){
        tokens[position] = token;
        position++;

        if( position >= bufsize ){
            bufsize += SHELL_TOK_BUFSIZE;
            tokens = realloc( tokens, bufsize * sizeof(char*) );
            if( !tokens ){
                err_dump("shell_split_line: allocation error");
            }
        }
        token = strtok( NULL, SHELL_TOK_DELIM );
    }

    tokens[position] = NULL;

    return tokens;
}

int shell_execute( char **args ){

    if( strcmp( args[0], "setenv" ) == 0 ){
        return setenvPATH( args );
    }

    if( strcmp( args[0], "printenv" ) == 0 ){
        return printenv( args );
    }

    if( strcmp( args[0], "exit" ) == 0 ){
        return 0;
    }

    return shell_launch_command( args );
}

int shell_launch_command( char **args ){
    pid_t pid, wpid;
    int status;

    pid = fork();
    if( pid == 0 ){

        int index = 0;
        while( args[index] != NULL ){
            if( strcmp( args[index], ">" ) == 0 ){
                redirection( args, index + 1 );
                return 1;
            }
            index++;
        }

        // /* child process */
        if( execvp( args[0], args ) == -1 ){
            write( STDERR_FILENO, "Unknown Command: [", strlen("Unknown Command: [") );
            write( STDERR_FILENO, args[0], strlen(args[0]) );
            write( STDERR_FILENO, "]\n", 2 );
        }
        exit( EXIT_FAILURE );
    }
    else if( pid < 0){
        /* fork failed */
        err_dump("shell_launch_command: fork failed");
    }
    else{
        /* parent process */
        do{
            wpid = waitpid( pid, &status, WUNTRACED );
        } while( !WIFEXITED(status) && !WIFSIGNALED(status) );
    }

    return 1;
}

/* Builtin function setenv */
int setenvPATH( char **args ){
    int n = strlen( args[1] ) + strlen( args[2] );
    char *env = (char*)malloc( sizeof(char) * n + 2 );
    memset( env, '\0', n + 2 );

    strcat( env, args[1] );
    strcat( env, "=" );
    strcat( env, args[2] );

    // printf( "%s", args[2] );
    putenv( env );

    return 2;
}

/* Builtin function printenv */
int printenv( char **args ){

    // print the env which user indicates
    write( STDOUT_FILENO, "PATH=", 5 );
    write( STDOUT_FILENO, getenv( args[1] ) , strlen( getenv( args[1] ) ) );
    write( STDOUT_FILENO, "\n", 1 );

    return 1;
}

void redirection( char **args, int filename_pos ){
    FILE *newfile;
    newfile = fopen( args[filename_pos], "w" );

    // move command to new array
    int argsLen = calArgsLen( args );
    char **command = (char**)malloc( sizeof(char*) * argsLen - 2 );
    for( int i = 0; i < ( argsLen - 3 ); i++ ){
        command[i] = args[i];
    }
    // write( STDOUT_FILENO, command[0], strlen( command[0] ) );
    command[ argsLen - 3 ] = NULL;

    dup2( fileno( newfile ), STDOUT_FILENO );

    if( execvp( command[0], command ) == -1 ){
        write( STDERR_FILENO, "Unknown Command: [", strlen("Unknown Command: [") );
        write( STDERR_FILENO, command[0], strlen(command[0]) );
        write( STDERR_FILENO, "]\n", 2 );
    }

    fclose( newfile );

    return;
}

/* calculate the amount of the elements in two dimensional pointer */
int calArgsLen( char **args ){
    int n = 0;
    while( args[n] != NULL ){
        n++;
    }

    return ( n + 1 );
}

/* deal with original pipe */
int dealpipe( char **commands, int commandNum, int table[][3], int tableRow, int zeroCounterRow, int exclamation ){
    int status = 1;
    int pid, pipefd[2];
    int passNum = 0;
    int fd_in = 0;
    int flag_to_process_number = tableRow;

    if( zeroCounterRow != -1 ){
        fd_in = table[zeroCounterRow][1];
        close( table[zeroCounterRow][0] );
    }

    // char a[30];
    // sprintf( a, "%d", commandNum );
    // write( STDOUT_FILENO, a, strlen(a) );

    // parse every command in commands array
    for( int i = 0; i < commandNum - 1; i++ ){
        char **args;
        args = shell_split_line( commands[i] );

        int len = calArgsLen( args );
        if( args[len - 2][0] == '!' ){
            args[len - 2] = NULL;
        }

        if( pipe(pipefd) < 0 ) err_dump("dealpipe: can't create pipes");

        pid = fork();

        if( (pid) < 0 ){
            err_dump("dealpip: can't fork");
        }
        else if( pid == 0 ){
            close( pipefd[0] );
            // close( pipefd[0] );

            dup2( fd_in, STDIN_FILENO );

            if( i != commandNum - 2 ){
                dup2( pipefd[1], STDOUT_FILENO );
                // dup2( pipefd[1], STDERR_FILENO );
            }
            else if( i == commandNum - 2 ){
                if( ( flag_to_process_number != -1 )  ){
                    if( exclamation == 1 ){
                        dup2( table[tableRow][0], STDERR_FILENO );
                        // char a[20];
                        // sprintf( a, "%d", table[tableRow][2] );
                    }
                    dup2( table[tableRow][0], STDOUT_FILENO );
                }
                // else{
                //     dup2( pipefd[1], STDOUT_FILENO );
                //     dup2( pipefd[1], STDERR_FILENO );
                // }
            }

            status = shell_execute( args );

            exit(status);
        }
        else{
            close( pipefd[1] );

            wait( &status );

            if( i > 0 ){
                close( fd_in );
            }

            fd_in = pipefd[0];

            if( status == 0 ){
                break;
            }
            else if( status == 512 ){
                status = shell_execute( args );
            }
        }
    }

    if( zeroCounterRow != -1 ){
        close( table[zeroCounterRow][1] );
        table[zeroCounterRow][2] = -1;
    }
    close( pipefd[0] );
    return status;
}

// initialize the pipe table
void initialPipeTable( int table[][3] ){
    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        table[i][2] = -1;
    }
}

// get the pipe number if it has
int containNum( char **commands, int commandNum ){
    char **args;
    int counter = 0;

    if( counter = atoi( commands[commandNum - 2] ) ){
        return counter;
    }

    return 0;
}

int findErrNum( char **commands, int commandNum ){
    char *line = (char*)malloc( strlen(commands[commandNum - 2]) * sizeof(char) + 1 );
    char **args;

    // memcpy( line, commands[commandNum - 2], strlen( commands[commandNum - 2] ) + 1 );
    strcpy( line, commands[commandNum - 2] );
    args = shell_split_line( line );

    int len = calArgsLen( args );
    int counter = 0;
    char *num = (char*)malloc( sizeof(char) * strlen( args[len - 2] ) );

    if( args[len - 2][0] == '!' ){
        memmove( &args[len - 2][0], &args[len - 2][1], strlen( args[len - 2] ) );
        counter = atoi( args[len - 2] );
        return counter;
    }

    free( line );
    return counter;
}


// return the first counter which is -1
int discoverEmptyEntryInTable( int table[][3] ){
    int row = -1;

    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( table[i][2] == -1 ){
            row = i;
            return row;
        }
    }
    return row;
}

// return the row of the same counter
int findSameCounter( int table[][3], int counter ){
    int row = -1;

    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( counter == table[i][2] ){
            row = i;
            return row;
        }
    }

    return row;
}

// substract every counter in the table
void subOneToCounter( int table[][3] ){
    for( int i = 0; i < MAX_PIPE_NUM; i++ ){
        if( table[i][2] >= 0 ){
            table[i][2]-=1;
        }
    }
}
